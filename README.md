# playbook-misc

Random Ansible example playbooks

## rhaap-wipe-inventory-vars

Use this playbook to remove extra variables defined on an inventory. This is a helper script to fix some messed up installation of Ansible Automation Platform Controller.

Set Controller environment variables first:

```bash
CONTROLLER_HOST=https://aap.example.com/
CONTROLLER_PASSWORD=secret
CONTROLLER_USERNAME=admin
CONTROLLER_VERIFY_SSL=false
```

Run the Playbook simply by using ansible-navigator or ansible-playbook:

```bash
ansible-navigator run rhaap-wipe-inventory-vars.yml
```

## deploy-demo-instance.yml

- log into Quay

```bash
podman login quay.io -u rhn_gps_cbolz
```

- source environment variables, e.g.

```bash
. ~/git/gitlab/ansible-inventory/variables/rhaap-demo/env.sh
```

- deploy instance

```bash
ansible-navigator run deploy-demo-instance.yml -e @/home/chris/git/gitlab/ansible-inventory/variables/rhaap-swisscom/main.yml --vault-password-file /home/chris/.vault -e @/home/chris/git/gitlab/ansible-inventory/variables/rhaap-swisscom/vault.yml -i /etc/ansible/hosts
```
